//* This file is part of the MOOSE framework
//* https://www.mooseframework.org
//*
//* All rights reserved, see COPYRIGHT for full restrictions
//* https://github.com/idaholab/moose/blob/master/COPYRIGHT
//*
//* Licensed under LGPL 2.1, please see LICENSE for details
//* https://www.gnu.org/licenses/lgpl-2.1.html

#pragma once

#include "IntegratedBC.h"

// Forward Declarations
class DGDirichBC;

template <>
InputParameters validParams<DGDirichBC>();

class DGDirichBC : public IntegratedBC
{
public:
  
  DGDirichBC(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual() override;
  virtual Real computeQpJacobian() override;

private:
 
  Real _epsilon;
  Real _sigma;
  Real _bcval;
  const MaterialProperty<Real> & _kappa;
};
