//* This file is part of the MOOSE framework
//* https://www.mooseframework.org
//*
//* All rights reserved, see COPYRIGHT for full restrictions
//* https://github.com/idaholab/moose/blob/master/COPYRIGHT
//*
//* Licensed under LGPL 2.1, please see LICENSE for details
//* https://www.gnu.org/licenses/lgpl-2.1.html

#pragma once

#include "DGKernel.h"

// Forward Declarations
class DGDiff;

template <>
InputParameters validParams<DGDiff>();


class DGDiff : public DGKernel
{
public:
  DGDiff(const InputParameters & parameters);

protected:
  virtual Real computeQpResidual(Moose::DGResidualType type) override;
  virtual Real computeQpJacobian(Moose::DGJacobianType type) override;

  Real _epsilon;
  Real _sigma;
  const MaterialProperty<Real> & _kappa;
  const MaterialProperty<Real> & _kappa_neighbor;
private:
  /// Broken boundaries
  std::set<BoundaryID> _excluded_boundaries;

  /// Check current element if it contains broken boundary
  bool excludeBoundary() const;
};

