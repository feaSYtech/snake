[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator 
    dim = 2
    nx = 40
    ny = 40
    xmax = 1
    ymax = 1
    #uniform_refine = 2
  [../]
[]

[Variables]
  active = 'u'

  [./u]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Kernels]
  active = 'diff'

  [./diff]
    type = CoefDiffusion
    variable = u
    kappa = 81
  [../]
[]

[BCs]
  active = 'left right'

  [./left]
    type = DirichletBC
    variable = u
    boundary = 'right left bottom'
    value = 100
  [../]

  [./right]
    type = DirichletBC
    variable = u
    boundary = 'top'
    value = 20
  [../]
[]

[Executioner]
  type = Steady

  solve_type = 'NEWTON'
[]

[Outputs]
  file_base = out
  exodus = true
[]
  
