[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator
    dim = 3
    nx = 5
    ny = 20
    nz = 5
    xmax = 2
    ymax = 2
    zmax = 2
    #uniform_refine = 2
  []
  [./subdomain]
    type = SubdomainBoundingBoxGenerator
    input = gmg
    bottom_left = '0 0 0'
    top_right = '2 1 2'
    block_id = 1
  [../]
  [./interface]
    type = SideSetsBetweenSubdomainsGenerator
    input = subdomain
    master_block = '1'
    paired_block = '0'
    new_boundary = 'interface0'
  [../]
  
[]

[Variables]
  # Defining a DFEM variable to handle gap discontinuity
  [T]
    order = FIRST
    family = MONOMIAL
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion
    variable = T
    kappa = 0.1
  [../]
  [./time]
    type = TimeDerivative
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff
    variable = T
    epsilon = -1
    sigma = 6
    kappa = 0.1
    exclude_boundary = 'interface0'
  [../]
[]

[InterfaceKernels]
  [./interface]
    type = InterfaceConvect
    variable = T
    neighbor_var = T
    boundary = interface0
    R = 100
  [../]
[]


[BCs]
  [top]
    type = DGDirichBC
    boundary = 'top'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=0
  []
  [bottom]
    type = DGDirichBC
    boundary = 'bottom'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=1
  []
[]
[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient    # 瞬态或稳态，可选 Transient | Steady
  num_steps = 100     # 计算步数
  dt = 0.1            # 时间步长
  
  solve_type = NEWTON   # 非线性求解方法，可选 NEWTON | PJFNK

  l_tol = 1e-03          # 线性迭代相对残值收敛判据
  l_max_its = 50         # 最大线性迭代步
   
  nl_max_its = 10        # 最大非线性迭代步
  nl_rel_tol = 1E-04     # 非线性迭代相对残值收敛判据

  petsc_options_iname = '-pc_type -pc_hypre_type'  # 预处理方法
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
