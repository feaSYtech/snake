[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator 
    dim = 2
    nx = 100
    ny = 100
    xmax = 2
    ymax = 2
    #uniform_refine = 2
  []
  [./subdomain]
    type = SubdomainBoundingBoxGenerator  
    input = gmg
    bottom_left = '0 0 0'
    top_right = '2 1 0'
    block_id = 1
  [../]
  [./interface]
    type = SideSetsBetweenSubdomainsGenerator  
    input = subdomain
    master_block = '1'
    paired_block = '0'
    new_boundary = 'interface0'             
  [../]
  
[]

[Variables]
  [./T]                  
    order = FIRST       
    family = MONOMIAL   
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion   
    variable = T 
    kappa = 0.1   
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff         
    variable = T
    epsilon = -1          
    sigma = 6   
    kappa = 0.1
    exclude_boundary = 'interface0'   
  [../]
[]

[InterfaceKernels]
  [./interface]
    type = InterfaceConvect  
    variable = T
    neighbor_var = T
    boundary = interface0   
    R = 1e2                 
  [../]
[]


[BCs]
  [top]
    type = DGDirichBC   
    boundary = 'top'    
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=0            
  []
  [bottom]
    type = DGDirichBC
    boundary = 'bottom'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=1
  []
[]


[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient     
  num_steps = 250     
  dt = 0.1           
  
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 50         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]



[Outputs]
  exodus = true
[]
