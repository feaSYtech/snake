[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator  
    dim = 2
    nx = 100
    ny = 100
    xmax = 2
    ymax = 2
    #uniform_refine = 2
  []
  [./subdomain]
    type = SubdomainBoundingBoxGenerator  
    input = gmg
    bottom_left = '0 0 0'
    top_right = '2 1 0'
    block_id = 1
  [../]
  [./interface]
    type = SideSetsBetweenSubdomainsGenerator 
    input = subdomain
    master_block = '0'
    paired_block = '1'
    new_boundary = 'interface0'            
  [../]
  
[]

[Variables]
  [./u]                   
    order = FIRST      
    family = LAGRANGE
    block=0  
  []
  [./v]                   
    order = FIRST      
    family = LAGRANGE  
    block=1
  []
[]


[Kernels]
  [./diff1]
    type = CoefDiffusion    
    variable = u 
    kappa = 0.1  
    block=0
  [../]
  [./diff2]
    type = CoefDiffusion    
    variable = v 
    kappa = 0.1  
    block=1
  [../]
  [./time]
    type = TimeDerivative   
    variable = u
  [../]
[]

[InterfaceKernels]
  [./interface]
    type = InterfaceConvect  
    variable = u
    neighbor_var = v
    boundary = interface0   
    R = 1e2                 
  [../]
[]


[BCs]
  [top]
    type = DirichletBC  
    boundary = 'top'    
    variable = u
    value = 0           
  []
  [bottom]
    type = DirichletBC
    boundary = 'bottom'
    variable = v
    value=1
  []
  
[]



[Executioner]
  type = Transient   
  num_steps = 200     
  dt = 0.1           
  
  solve_type = NEWTON   

  l_tol = 1e-03        
  l_max_its = 50         
   
  nl_max_its = 10       
  nl_rel_tol = 1E-04     
  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'
[]



[Outputs]
  exodus = true
[]
