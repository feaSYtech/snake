[Mesh]
 [./gmg]
    type = GeneratedMeshGenerator
    dim = 2
    nx = 20
    ny = 20
    xmax = 1
    ymax = 1
    #uniform_refine = 2
  []

  [./subdomain1]
    type = SubdomainBoundingBoxGenerator
    input = gmg
    bottom_left = '0 0 0'
    block_id = 1
    top_right = '0.5 1 0'
    
  []
  [./subdomain2]
    type = SubdomainBoundingBoxGenerator
    input = subdomain1
    bottom_left = '0 0 0'
    block_id = 2
    top_right = '0.5 0.5 0'
  []
  [./interface1]
    type = SideSetsBetweenSubdomainsGenerator
    input = subdomain2
    master_block = '0'
    paired_block = '1'
    new_boundary = 'in_be1'
  []
  [./interface2]
    type = SideSetsBetweenSubdomainsGenerator
    input = interface1
    master_block = '0'
    paired_block = '2'
    new_boundary = 'in_be2'
  []
[]


[Variables]
  [./u]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Kernels]
  [./diff]
    type = CoefDiffusion
    variable = u
    coef = 0.1
  [../]
  [./time]
    type = TimeDerivative
    variable = u
  [../]
[]
[InterfaceKernels]
  [./interface1]
    type = InterfaceDiffusion
    variable = u
    neighbor_var = u
    boundary = in_be1
    D = 4
    D_neighbor = 2
  [../]
  [./interface2]
    type = PenaltyInterfaceDiffusion
    variable = u
    neighbor_var = u
    boundary = in_be2
    penalty=1e8
  [../]
[]


[BCs]
  [./left]
    type = DirichletBC
    variable = u
    boundary = 'left'
    value = 1
  [../]
  [./right]
    type = DirichletBC
    variable = u
    boundary = 'right'
    value = 0
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]

[Executioner]
  type = Transient
  num_steps = 20
  dt = 0.1
  solve_type = PJFNK
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
  print_linear_residuals = true
[]

[Debug]
  show_var_residual_norms = true
[]
