[MeshModifiers]
  [./scale]
    type = Transform
    transform = SCALE
    vector_value = '1e-3 1e-3 1e-3'
  [../]
[]
[Mesh]
  file = grids/sphere.exo
[]

[Variables]
  # Defining a DFEM variable to handle gap discontinuity
  [T]
    order = FIRST
    family = MONOMIAL
    initial_condition = 20
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion
    variable = T
  [../]
  [./time]
    type = SpecificTimeDerivative
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff
    variable = T
    epsilon = -1
    sigma = 6
  [../]
[]

[BCs]
  [plane]
    type = DGConvectBC   
    boundary = 'SPHERE_WALL'   
    variable = T
    hw=100
    Tw=100 
  []
[]
[Materials]
  [./const]
    type = GenericConstantMaterial
    prop_names='kappa rho cp'
    prop_values= '50 8000 500'
    block = 'FAM0-FREEPARTS_1_1_MATPOINT'
  [../]
[]
[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient
  scheme = 'bdf2'
  dt = 5
  num_steps = 200
  l_tol = 1e-04
  l_max_its = 20

  nl_max_its = 20
  nl_rel_tol = 1e-04

  solve_type = NEWTON
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
