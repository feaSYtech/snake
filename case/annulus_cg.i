[Mesh]
   file = grids/annulusf3.exo
[]

[Variables]
  [./u]                   
    order = FIRST      
    family = LAGRANGE
    block='OUTSIDE'  
  []
  [./v]                   
    order = FIRST      
    family = LAGRANGE  
    block='INSIDE'
  []
[]


[Kernels]
  [./diff1]
    type = CoefDiffusion    
    variable = u 
    kappa = 0.1  
    block='OUTSIDE'
  [../]
  [./diff2]
    type = CoefDiffusion    
    variable = v 
    kappa = 0.1  
    block='INSIDE'
  [../]
  [./time]
    type = TimeDerivative   
    variable = u
  [../]
[]

[InterfaceKernels]
  [./interface]
    type = InterfaceConvect  
    variable = u
    neighbor_var = v
    boundary = 'INTER'   
    R = 1e2                 
  [../]
[]


[BCs]
  [top]
    type = DirichletBC  
    boundary = 'OUT'    
    variable = u
    value = 0           
  []
  [bottom]
    type = DirichletBC
    boundary = 'IN'
    variable = v
    value=1
  []
  
[]



[Executioner]
  type = Transient   
  num_steps = 200     
  dt = 0.1           
  
  solve_type = NEWTON   

  l_tol = 1e-03        
  l_max_its = 50         
   
  nl_max_its = 10       
  nl_rel_tol = 1E-04     
  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'
[]



[Outputs]
  exodus = true
[]
