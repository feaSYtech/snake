[Mesh]
  file = mesh-only_in.e
[]

[MeshModifiers]
  [interface]
    type = SideSetsBetweenSubdomains
    master_block = 0
    paired_block = 1
    new_boundary = 'interface'
  [../]
[]

[Variables]
  [./u]
  [../]
[]

[Kernels]
  [./diff]
    type = DiffusionKernel
    variable = u
  [../]
[]

[InterfaceKernels]
	[crt]
	  type = CTRInterface
	  kb = 0
	  kf = 0
    R = 0
	  neighbor_var = u
	  variable = u
	  boundary = interface
	[]
[]


[BCs]
  [./left]
    type = DirichletBC
    variable = u
    boundary = left
    value = 0
  [../]
  [./right]
    type = DirichletBC
    variable = u
    boundary = right
    value = 1
  [../]
[]

[Executioner]
  type = Steady
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
