[Mesh]
  file = grids/annulusf3.exo
[]

[Variables]
  [./T]                  
    order = FIRST       
    family = MONOMIAL   
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion   
    variable = T 
    kappa = 0.1   
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff         
    variable = T
    epsilon = -1          
    sigma = 4   
    kappa = 0.1
    exclude_boundary = 'INTER'   
  [../]
[]

[InterfaceKernels]
  [./interface]
    type = InterfaceConvect  
    variable = T
    neighbor_var = T
    boundary = INTER   
    R = 1e2                 
  [../]
[]


[BCs]
  [top]
    type = DGDirichBC   
    boundary = 'OUT'    
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 4
    value=0            
  []
  [bottom]
    type = DGDirichBC
    boundary = 'IN'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 4
    value=1
  []
[]


[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient     
  num_steps = 2000     
  dt = 0.1           
  
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 50         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]
[Outputs]
  exodus = true
[]
