[MeshModifiers]
  [./scale]
    type = Transform
    transform = SCALE
    vector_value = '1e-3 1e-3 1e-3'
  [../]
[]
[Mesh]
  file = grids/cylindrical03.exo
[]

[Variables]
  # Defining a DFEM variable to handle gap discontinuity
  [T]
    order = FIRST
    family = MONOMIAL
    initial_condition = 25
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion
    variable = T
  [../]
  [./time]
    type = SpecificTimeDerivative
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff
    variable = T
    epsilon = -1
    sigma = 6
  [../]
[]



[BCs]
  [plane]
    type = DGNeumaBC   
    boundary = 'WALL_BOTTOM WALL_TOP WALL_CYLINDRICAL'   
    variable = T
    #hw=20
    #Tw=800 
    therflux=800
  []
[]
[Materials]
  [./const]
    type = GenericConstantMaterial
    prop_names='kappa rho cp'
    prop_values= '58 8000 545'
    block = 'CYLINDRICAL_BODY'
  [../]
[]
[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]
[Postprocessors]
  [./elapsed]
    type = PerfGraphData
    section_name = "Root"
    data_type = self
  [../]
[]

[Executioner]
  type = Transient

  num_steps = 200
  scheme = 'bdf2'
  dt = 100
  l_tol = 1e-04
  l_max_its = 20

  nl_max_its = 10
  nl_rel_tol = 1e-04

  solve_type = NEWTON
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
