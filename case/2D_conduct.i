[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator 
    dim = 2
    nx = 80
    ny = 80
    xmax = 0.833
    ymax = 0.83
    #uniform_refine = 2
  []
  
[]

[Variables]
  [./T]                   
    order = FIRST       
    family = MONOMIAL   
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion    
    variable = T  
    block = 0 
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff         
    variable = T
    epsilon = -1          
    sigma = 5   
  [../]
[]


[BCs]
  [right]
    type = DGDirichBC   
    boundary = 'bottom left right '    
    variable = T
    epsilon = -1
    sigma = 5
    value=100             
  []
  [left]
    type = DGDirichBC
    boundary = 'top'
    variable = T
    epsilon = -1
    sigma = 5
    value=20   
  []
[]
[Materials]
  [./const]
    type = GenericConstantMaterial
    prop_names='kappa'
    prop_values=81
    block = 0
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient    
  num_steps = 100     
  dt = 0.01           
  
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 50         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]



[Outputs]
  exodus = true
[]

