[Mesh]
 [./gmg]
    type = GeneratedMeshGenerator 
    dim = 2
    nx = 80
    ny = 20
    xmin = 0
    xmax = 0.5
    ymax = 0.2
    #uniform_refine = 2
  []
[]

[Variables]
  [./T]                   
    order = FIRST        
    family = LAGRANGE   
  []
[]
[AuxVariables]
  [./from_sub]
    order = FIRST        
    family = LAGRANGE 
  []
[]
[Kernels]
  [./diff]
    type = CoefDiffusion    
    variable = T 
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]
[BCs]
  [./left]
    type = DirichletBC
    variable = T
    boundary = 'left'
    value = 20
  [../]
  [./right]
    type = DirichletBC
    variable = T
    boundary = 'right'
    value = 100
  [../]
[]
[Materials]
  [./const0]
    type = GenericConstantMaterial
    prop_names='kappa'
    prop_values=81
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient    
  num_steps = 5     
  dt = 0.01           
  
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 50         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]


[Outputs]
  exodus = true
[]
[MultiApps]
  [sub]
    positions = '0 0 0'
    type = TransientMultiApp
    app_type = snakeApp
    input_files = 'mtos_sub.i'
  []
[]

