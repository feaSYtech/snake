[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator 
    dim = 2
    nx = 5
    ny = 5
    xmax = 1
    ymax = 1
    #uniform_refine = 2
  []
  
[]

[Variables]
  [./T]                   
    order = FIRST       
    family = MONOMIAL   
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion    
    variable = T   
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff         
    variable = T
    epsilon = -1          
    sigma = 5   
  [../]
[]
[Functions]
  [./parsed_function]
    type = ParsedFunction
    value = 'sin(pi*x)*sin(pi*y)+sin(2*pi*x)*sin(2*pi*y)'
  [../]
[]
[ICs]
  [./T_ic]
    type = FunctionIC
    variable = 'T'
    function = parsed_function
  [../]
[]
[BCs]
  [bc]
    type = DGDirichBC   
    boundary = 'bottom left right top'    
    variable = T
    epsilon = -1
    sigma = 5
    value=0             
  []
[]
[Materials]
  [./const]
    type = GenericConstantMaterial
    prop_names='kappa'
    prop_values=1
    block = 0
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]
[Postprocessors]
  [./error]
    type = ElementL2Error
    function = 'sin(pi*x)*sin(pi*y)*exp(-pi*pi*2*t)+sin(2*pi*x)*sin(2*pi*y)*exp(-4*pi*pi*2*t)'
    variable = T
  [../]
[]

[Executioner]
  type = Transient    
  num_steps = 1000     
  dt = 1e-4           
  scheme = 'bdf2'
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 50         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]



[Outputs]
  exodus = true
[]

