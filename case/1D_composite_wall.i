[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator  
    dim = 2
    nx = 80
    ny = 40
    xmax = 0.07
    ymax = 0.02
    #uniform_refine = 2
  []
  [./subdomain1]
    type = SubdomainBoundingBoxGenerator 
    input = gmg
    bottom_left = '0 0 0'
    top_right = '0.05 0.02 0'
    block_id = 1
  [../]
[]

[Variables]
  [./T]                   
    order = FIRST        
    family = MONOMIAL   
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion    
    variable = T 
  [../]
  [./heatforce]
    type = BodyForce    
    variable = T 
    function = volumetric_heat
    block=1
  [../]
  [./time]
    type = TimeDerivative   
    variable = T
  [../]
[]
[Functions]
  [./volumetric_heat]
     type = ParsedFunction
     value = 1.5e+6
  [../]
[]
[DGKernels]
  [./dg_diff]
    type = DGDiff         
    variable = T
    epsilon = -1          
    sigma = 4   
  [../]
[]
[BCs]
  [right]
    type = DGConvectBC   
    boundary = 'right'    
    variable = T
    hw=1000
    Tw=30          
  []
[]
[Materials]
  [./const0]
    type = GenericConstantMaterial
    prop_names='kappa'
    prop_values=150
    block = 0
  [../]
  [./const1]
    type = GenericConstantMaterial
    prop_names='kappa'
    prop_values=75
    block = 1
  [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient    
  num_steps = 1000     
  dt = 0.0001            
  
  solve_type = NEWTON   

  l_tol = 1e-03          
  l_max_its = 10         
   
  nl_max_its = 10        
  nl_rel_tol = 1E-04     

  petsc_options_iname = '-pc_type -pc_hypre_type'  
  petsc_options_value = 'hypre boomeramg'

[]



[Outputs]
  exodus = true
[]
