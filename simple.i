[Mesh]
  [./gmg]
    type = GeneratedMeshGenerator
    dim = 2
    nx = 20
    ny = 20
    xmax = 1
    ymax = 1
    #uniform_refine = 2
  []

  [./subdomain1]
    type = SubdomainBoundingBoxGenerator
    input = gmg
    bottom_left = '0 0 0'
    block_id = 1
    top_right = '0.8 1 0'
    
  []
  [./subdomain2]
    type = SubdomainBoundingBoxGenerator
    input = subdomain1
    bottom_left = '0 0 0'
    block_id = 2
    top_right = '0.8 0.5 0'
  []
  [./subdomain3]
    type = SubdomainBoundingBoxGenerator
    input = subdomain2
    bottom_left = '0 0 0'
    block_id = 3
    top_right = '0.8 0.4 0'
  []
  [./interface1]
    type = SideSetsBetweenSubdomainsGenerator
    input = subdomain3
    master_block = '0'
    paired_block = '1 3'
    new_boundary = 'in_be1'
  []
  [./interface2]
    type = SideSetsBetweenSubdomainsGenerator
    input = interface1
    master_block = '0'
    paired_block = '2'
    new_boundary = 'in_be2'
  []
  
[]

[Variables]
  [./u]
  [../]
[]

[Kernels]
  [./diff0]
    type = CoefDiffusion
    variable = u
    coef = 1
    block = 0
  [../]
  [./diff1]
    type = CoefDiffusion
    variable = u
    coef = 1
    block = '1 2 3'
  [../]
  [./time]
    type = TimeDerivative
    variable = u
  [../]
[]
[InterfaceKernels]
  [./interface1]
    type = InterfaceConvect
    variable = u
    neighbor_var = u
    boundary = in_be2
    h=4e8
  [../]
  [./interface2]
    type = InterfaceConvect
    variable = u
    neighbor_var = u
    boundary = in_be1
    h=4e8
  [../]
[]
[BCs]
  [./left]
    type = DirichletBC
    variable = u
    boundary = left
    value = 0
  [../]
  [./right]
    type = DirichletBC
    variable = u
    boundary = right
    value = 1
  [../]
[]
[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]
[Executioner]
  type = Transient
  num_steps = 40
  dt = 0.1
  solve_type = PJFNK
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
