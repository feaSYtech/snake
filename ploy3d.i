[Mesh]
  [./gmg]
    type = FileMeshGenerator
    file = grids/poly2.msh
    #uniform_refine = 2
  []
  [./interface1]
    type = SideSetsBetweenSubdomainsGenerator
    input = gmg
    master_block = '10 1'
    paired_block = '6'
    new_boundary = 'inface1'
  [../]
  [./interface2]
    type = SideSetsBetweenSubdomainsGenerator
    input = interface1
    master_block = '4'
    paired_block = '3'
    new_boundary = 'inface2'
  [../]
  
[]

[Variables]
  # Defining a DFEM variable to handle gap discontinuity
  [T]
    order = FIRST
    family = MONOMIAL
  []
[]


[Kernels]
  [./diff]
    type = CoefDiffusion
    variable = T
    kappa = 0.1
  [../]
  [./time]
    type = TimeDerivative
    variable = T
  [../]
[]

[DGKernels]
  [./dg_diff]
    type = DGDiff
    variable = T
    epsilon = -1
    sigma = 6
    kappa = 0.1
    exclude_boundary = 'inface1 inface2'
  [../]
[]

[InterfaceKernels]
  [./interface1]
    type = InterfaceConvect
    variable = T
    neighbor_var = T
    boundary = inface1
    R = 1e2
  [../]
  [./interface2]
    type = InterfaceConvect
    variable = T
    neighbor_var = T
    boundary = inface2
    R = 1e2
  [../]
[]


[BCs]
  [left]
    type = DGDirichBC
    boundary = '10 17 47 53'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=0
  []
  [right]
    type = DGDirichBC
    boundary = '24 31 38 44'
    variable = T
    kappa = 0.1
    epsilon = -1
    sigma = 6
    value=1
  []
[]
[Preconditioning]
  [./smp]
    type = SMP
    full = true
  [../]
[]


[Executioner]
  type = Transient
  num_steps = 100
  dt = 0.1
  l_tol = 1e-04
  l_max_its = 50

  nl_max_its = 20
  nl_rel_tol = 1e-04

  solve_type = PJFNK
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Outputs]
  exodus = true
[]
